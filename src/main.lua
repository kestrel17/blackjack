require("card")

WIDE = 600
HIGH = 600
pad = WIDE*0.04
scale = 0.33
suits = {"hearts", "spades", "clubs", "diamonds"}
values = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack",
  "King", "Queen"}
hand = {}
total = 0 --player score
comp = {} --computer hand

love.window.setTitle('Blackjack')
love.window.setMode(WIDE, HIGH)

function love.load()
end

local card = Card.init("hearts",8)
print(card.suit .. " " .. card.value)

